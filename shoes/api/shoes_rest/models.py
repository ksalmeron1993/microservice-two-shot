from django.db import models
from django.urls import reverse



# Create your models here.
class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length= 100)

    def __str__(self):
            return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")



class Shoe(models.Model):
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    model = models.CharField(max_length=100,null=True)
    picture_url = models.URLField(null=True, blank=True)
    
    bin = models.ForeignKey(
        'BinVO', 
        related_name='shoes', 
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.model