# Wardrobify

Team:

* Kevin Salmeron - Shoes
* Michael Maloney - Hats

Design
Going to start and finish with backend first.   Then we can build out react components.
First step istalling django app to django projec tinin stall apps part

Shoes microservice
Explain your models and integration with the wardrobe
microservice, here.

Hats microservice

There a total of 7 containers in this application
microservice-two-shot-react-1 - Port: 3000
microservice-two-shot-database-1 Port: 15432
microservice-two-shot-shoes-poller-1
microservice-two-shot-hats-poller-1
microservice-two-shot-hats-api-1 Port: 8090
microservice-two-shot-shoes-poller-1 Port: 8080
microservice-two-shot-wardrobe-api Port: 8100
To build the database, image, and containers run the following commands:
docker volume create pgdata
docker-compose build
docker-compose up
SAMPLA DATA:
Create Location:
{
"closet_name" : "Closet 1",
"section_number" : 2,
"shelf_number" : 2
}
Create Hat:
{
"style" : "Carhartt",
"color" : "Brown",
"fabric" : "Cotton",
"picture_url" : "https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcRV49yA0lrj8NtIup_d97P0ISrkE5IgN2GkVJZ2Ojx9AIASDOM8He2K-ZDW5bZxQKxWmrPPv4tt5ZQ&usqp=CAc",
"location" : 1
}
URLS:
Location urls:
GET all locations - http://localhost:8100/api/locations
GET 1 location - http://localhost:8100/api/locations/1
POST a location - http://localhost:8100/api/locations/
DELETE a location - http://localhost:8100/api/locations/1
Hat urls:
GET all hats - http://localhost:8090/api/hats
GET 1 hat - http://localhost:8090/api/hats/1
POST a hat - http://localhost:8090/api/hats/
DELETE a hat - http://localhost:8090/api/hats/1
