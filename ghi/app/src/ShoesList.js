import React, {useEffect, useState} from 'react'

function ShoesList(props) {
/////This code refers to the delete button. You will see an onclick listener inside our jsx.//
///// This is so when we delete a "hat" it automatically "refreshes" the page for us.
    const deleteItem = async(hat) => {
        const shoeURL = `http://localhost:8080/<id:pk>`;
        const fetchConfig = {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(shoeURL, fetchConfig);
        console.log(response)
        // This is the two lines of code that refreshes our page for us
        if (response.ok) {
            props.fetchShoes();
        }
    }

//Our JSX that renders our table
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Manufacturer</th>
                    <th>Bin</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {/* This is like using a "for-loop" in JSX. It populates the rows of our table with our data by passing through the prop. See
                in the app.js  */}
                {props.hats.map(shoe => {
                return (
                    <tr key={shoe.model_name}>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model }</td>
                        <td>{ shoe.picture_url }</td>
                        <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(shoe)}>Delete!</button> </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }

export default ShoesList;



