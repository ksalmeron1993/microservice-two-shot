import {useEffect, useState} from "react"
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './Hats';
import NewHatForm from './NewHatForm';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])


  const fetchShoes = async () => {
    const url = "http://localhost:8080/api/shoes/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setShoes(data.shoes)
    }
  }

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }
  }

  useEffect(() => {
    fetchShoes();
    getHats();
  }, [])
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<NewHatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
  }
export default App;
