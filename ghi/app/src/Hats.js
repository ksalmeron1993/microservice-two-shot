import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

const HatsList = () => {
    const [hats, setHats] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/hats/')
            .then(response => response.json())
            .then(data => {
                setHats(data.hats);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const onDeleteHatClick = (hat) => {
        const hatHref = hat.href;
        const hrefComponents = hat.href.split('/');
        const pk = hrefComponents[hrefComponents.length - 2];
        const hatsUrl = `http://localhost:8090/api/hats/${pk}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(hatsUrl, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const currentHats = [...hats];
                    setHats(currentHats.filter(hat => hat.href !== hatHref));
                }
            })
            .catch(e => console.log('error: ', e));
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Location</th>
                        <th>Pic</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.href}>
                                <td>
                                    <button onClick={() => onDeleteHatClick(hat)}>X</button>
                                    <span>{hat.style}</span>
                                </td>
                                <td>{hat.color}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.location.closet_name}</td>
                                <td>
                                    <img className="hat-image" src={hat.pic_url} />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/hats/new"}>Create new hat</Link>
        </>
    );
};

export default HatsList;
