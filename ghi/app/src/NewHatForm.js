import React, { useEffect, useState } from 'react';

const NewHatForm = () => {

    const [hatColor, setHatColor] = useState('');
    const [hatStyle, setHatStyle] = useState('');
    const [hatFabric, setHatFabric] = useState('');
    const [hatPicUrl, setHatPicUrl] = useState('');
    const [hatLocation, setHatLocation] = useState('');
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        const locationVOsURL = 'http://localhost:8100/api/locations/';
        fetch(locationVOsURL)
            .then(response => response.json())
            .then(data => setLocations(data.locations))
            .catch(e => console.error('error: ', e))
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newHat = {
            'style': hatStyle,
            'color': hatColor,
            'fabric': hatFabric,
            'pic_url': hatPicUrl,
            'location_id': hatLocation
        }

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newHat),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(hatsUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setHatColor('');
                setHatStyle('');
                setHatFabric('');
                setHatPicUrl('');
                setHatLocation('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setHatStyle(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setHatColor(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setHatFabric(value);
    }

    const handlePicUrlChange = (event) => {
        const value = event.target.value;
        setHatPicUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setHatLocation(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={hatStyle} onChange={handleStyleChange} required type="text" name="style" id="style" className="form-control" />
                            <label>Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatColor} onChange={handleColorChange} required type="text" name="color" id="color" className="form-control" />
                            <label>Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatFabric} onChange={handleFabricChange} required type="text" name="fabric" id="fabric" className="form-control" />
                            <label>Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={hatPicUrl} onChange={handlePicUrlChange} required type="text" name="picUrl" id="picUrl" className="form-control" />
                            <label>PicUrl</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={hatLocation} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.closet_name}</option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewHatForm;
